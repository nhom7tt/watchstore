package com.example.APIGateWay.jwtUtils;

import java.util.List;
import java.util.function.Predicate;

import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;

@Component
public class RouteValid {
	public static final List<String> listApiAuthen=List.of(
			"/auth/**"
			);
	public Predicate<ServerHttpRequest> isSecured= 
			request-> listApiAuthen
						.stream()
						.noneMatch(uri->request.getURI().getPath().contains(uri));
}
