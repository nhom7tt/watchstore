//package com.example.APIGateWay.config;
//
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
//import org.springframework.security.config.web.server.SecurityWebFiltersOrder;
//import org.springframework.security.config.web.server.ServerHttpSecurity;
//import org.springframework.security.web.SecurityFilterChain;
//import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
//import org.springframework.security.web.server.SecurityWebFilterChain;
//import com.example.APIGateWay.jwtUtils.JwtAuthenticationFilter;
//
//@Configuration
//
//public class SecurityConfig  {
//	private JwtAuthenticationFilter authenticationFilter () {
//		return new JwtAuthenticationFilter();
//	}
//	
//	@Bean
//	public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
//		http.csrf(csrf -> csrf.disable())
//				.authorizeHttpRequests(authorize -> authorize.requestMatchers("/auth/**").permitAll()
//						.requestMatchers("/customer/**").permitAll().requestMatchers("/watch/**").permitAll()
//						.requestMatchers("/orders/**").permitAll().anyRequest().authenticated())
//				.formLogin();
//
//		http.addFilterBefore(authenticationFilter(), UsernamePasswordAuthenticationFilter.class);
//		return http.build();
//	}
//}
