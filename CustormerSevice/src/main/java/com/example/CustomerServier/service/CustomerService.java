package com.example.CustomerServier.service;

import java.util.List;

import com.example.CustomerServier.entity.Customer;

public interface CustomerService   {
	List<Customer> findAllCustomers();
    Customer addCustomer(Customer customer);
    Customer updateCustomer(Customer customer);
    void deleteCustomer(int id);
    Customer findCustomerById(int id);
}
