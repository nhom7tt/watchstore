package com.example.CustomerServier.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.CustomerServier.entity.Customer;
import com.example.CustomerServier.reponsitory.CustomerReponsitory;
@Service
public class CustomerServiceImlp implements CustomerService {
	@Autowired
	CustomerReponsitory customerReponsitory;
	@Override
	public List<Customer> findAllCustomers() {
		// TODO Auto-generated method stub
		List<Customer> customers = new ArrayList<>();
		customers = customerReponsitory.findAll();
		return customers;
	}

	@Override
	public Customer addCustomer(Customer customer) {
		return customerReponsitory.saveAndFlush(customer);
		
	}

	@Override
	public Customer updateCustomer(Customer customer) {
        Optional<Customer> existingCustomerOptional = customerReponsitory.findById(customer.getId());
        if (existingCustomerOptional.isPresent()) {
            Customer existingCustomer = existingCustomerOptional.get();
            existingCustomer.setTenkhachhang(customer.getTenkhachhang());
            existingCustomer.setDiachi(customer.getDiachi());
            existingCustomer.setPhone(customer.getPhone());
            return customerReponsitory.saveAndFlush(existingCustomer);
        } else {
            throw new RuntimeException("Watch  id " + customer.getId() + " khong tim thay");
        }
    }

	@Override
	public void deleteCustomer(int id) {
		// TODO Auto-generated method stub
	customerReponsitory.deleteById(id);
	}

	@Override
	public Customer findCustomerById(int id) {
		// TODO Auto-generated method stub
		return customerReponsitory.findById(id).orElse(null);
	}

}
