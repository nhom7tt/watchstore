package com.example.CustomerServier.reponsitory;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.CustomerServier.entity.Customer;
@Repository
public interface CustomerReponsitory  extends JpaRepository<Customer, Integer>{

}
