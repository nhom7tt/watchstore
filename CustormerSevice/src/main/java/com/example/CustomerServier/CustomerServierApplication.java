package com.example.CustomerServier;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomerServierApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomerServierApplication.class, args);
	}

}
