package com.example.CustomerServier.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.example.CustomerServier.entity.Customer;
import com.example.CustomerServier.reponsitory.CustomerReponsitory;
import com.example.CustomerServier.service.CustomerService;

@RestController
@RequestMapping("/customer")
public class Controller {
	@Autowired
	private CustomerService customerService;
	@Autowired
	private RestTemplate restTemplate;
	@Autowired
	private CustomerReponsitory customerReponsitory;
	
	@RequestMapping(value = "/getCustomer" , method = RequestMethod.GET)
	public List<Customer> getAllCustomers(){
		return customerService.findAllCustomers();
		}
	
	
	@RequestMapping(value = "/postCustomer", method = RequestMethod.POST)
	public Customer addCustomer(@RequestBody Customer customer) {
		return customerService.addCustomer(customer);
	}
	
	@RequestMapping(value = "/putCustomer", method = RequestMethod.PUT)
	public Customer updateWatch(@RequestBody Customer customer) {
		return customerService.updateCustomer(customer);
	}
	
	@RequestMapping(value = "/deleteCustomer/{id}", method = RequestMethod.DELETE)
	public void deleteCustomer(@PathVariable int id) {
			customerService.deleteCustomer(id);
			
	}
	@RequestMapping(value = "/findCustomer/{id}", method = RequestMethod.GET)
	public Customer findCustomer(@PathVariable int id) {
		return customerService.findCustomerById(id);
	}
	

}
