package com.example.APIGateWay.jwtUtils;

import java.util.Date;

import javax.crypto.SecretKey;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;

@Component
public class JwtUtils {

	@Value("${jwt.secret}")
	private String secret;

	private SecretKey getSigningKey() {
		return Keys.hmacShaKeyFor(secret.getBytes());
	}

	public String createJwtToken(int userId) {
		return Jwts.builder().setSubject(String.valueOf(userId)).setIssuedAt(new Date())
				.setExpiration(new Date(System.currentTimeMillis() + 86400000)) // 24 hours
				.signWith(getSigningKey()).compact();
	}

	public int getUserIdFromJwtToken(String token) {
		Claims claims = Jwts.parserBuilder().setSigningKey(getSigningKey()).build().parseClaimsJws(token).getBody();
		return Integer.parseInt(claims.getSubject());
	}

	public void validateJwtToken(String authToken) {

		Jwts.parserBuilder().setSigningKey(getSigningKey()).build().parseClaimsJws(authToken);
	}
}
