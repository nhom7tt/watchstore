package com.example.UserService.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.example.UserService.entity.User;
import com.example.UserService.repository.UserRepository;

import jakarta.transaction.Transactional;


@Service
public class UserDetailServiceIml  implements UserDetailsService {
	@Autowired
    UserRepository userRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        int userId = Integer.parseInt(username); // Chuyển đổi use_id từ String sang int

        User user = userRepository.findById(userId)
                .orElseThrow(() -> new UsernameNotFoundException("User Not Found with user_id: " + userId));

        return UserDetailIml.build(user);
    }
}
