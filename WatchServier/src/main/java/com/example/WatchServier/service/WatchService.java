package com.example.WatchServier.service;

import java.util.List;

import com.example.WatchServier.entity.Watch;

public interface WatchService   {
	List<Watch> findAllWatchs();
    Watch addWatchs(Watch watch);
    Watch updateWatch(Watch watch);
    void deleteWatch(int id);
    Watch findWatchById(int id);
}
