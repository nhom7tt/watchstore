package com.example.WatchServier.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.WatchServier.entity.Watch;
import com.example.WatchServier.reponsitory.WatchReponsitory;
@Service
public class WatchServiceImlp implements WatchService {
	@Autowired
	WatchReponsitory watchReponsitory;
	@Override
	public List<Watch> findAllWatchs() {
		// TODO Auto-generated method stub
		List<Watch> watchs = new ArrayList<>();
		watchs = watchReponsitory.findAll();
		return watchs;
	}

	@Override
	public Watch addWatchs(Watch watch) {
		
		return watchReponsitory.saveAndFlush(watch);
		
	}

	@Override
	public Watch updateWatch(Watch watch) {
        Optional<Watch> existingWatchOptional = watchReponsitory.findById(watch.getId());
        if (existingWatchOptional.isPresent()) {
            Watch existingWatch = existingWatchOptional.get();
            existingWatch.setTensampham(watch.getTensampham());
            existingWatch.setSoluong(watch.getSoluong());
            existingWatch.setNhasanxuat(watch.getNhasanxuat());
            existingWatch.setGia(watch.getGia());
            return watchReponsitory.saveAndFlush(existingWatch);
        } else {
            throw new RuntimeException("Watch  id " + watch.getId() + " khong tim thay");
        }
    }

	@Override
	public void deleteWatch(int id) {
		// TODO Auto-generated method stub
		watchReponsitory.deleteById(id);
	}

	@Override
	public Watch findWatchById(int id) {
		// TODO Auto-generated method stub
		return watchReponsitory.findById(id).orElse(null);
	}

}
