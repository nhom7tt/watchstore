package com.example.WatchServier.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.example.WatchServier.entity.Watch;
import com.example.WatchServier.reponsitory.WatchReponsitory;
import com.example.WatchServier.service.WatchService;

@RestController
@RequestMapping("/watchs")
public class Controller {
	@Autowired
	private WatchService watchService;
	@Autowired
	private RestTemplate restTemplate;
	@Autowired
	private WatchReponsitory watchReponsitory;
	
	@RequestMapping(value = "/getWatch" , method = RequestMethod.GET)
	public List<Watch> getAllWatchs(){
		return watchService.findAllWatchs();
		}
	
	
	@RequestMapping(value = "/postWatch", method = RequestMethod.POST)
	public Watch addWatch(@RequestBody Watch watch) {
		return watchService.addWatchs(watch);
	}
	
	@RequestMapping(value = "/putWatch", method = RequestMethod.PUT)
	public Watch updateWatch(@RequestBody Watch watch) {
		return watchService.updateWatch(watch);
	}
	
	@RequestMapping(value = "/deleteWatch/{id}", method = RequestMethod.DELETE)
	public void deleteWatch(@PathVariable int id) {
			watchService.deleteWatch(id);
			
	}
	@RequestMapping(value = "/findWatch/{id}", method = RequestMethod.GET)
	public Watch findWatch(@PathVariable int id) {
		return watchService.findWatchById(id);
	}
	

}
