package com.example.WatchServier.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "watch")

public class Watch {
@Id
@GeneratedValue(strategy = GenerationType.AUTO)
 private int id;
 private String tensampham;
 private int soluong;
 private  String nhasanxuat;
 private  float gia;
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getTensampham() {
	return tensampham;
}
public void setTensampham(String tensampham) {
	this.tensampham = tensampham;
}
public int getSoluong() {
	return soluong;
}
public void setSoluong(int soluong) {
	this.soluong = soluong;
}
public String getNhasanxuat() {
	return nhasanxuat;
}
public void setNhasanxuat(String nhasanxuat) {
	this.nhasanxuat = nhasanxuat;
}
public float getGia() {
	return gia;
}
public void setGia(float gia) {
	this.gia = gia;
}
 
 

 
}
