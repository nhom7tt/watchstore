package com.example.OrderService.entity;
public class CustomerDTO {
    private int id;
    private String tenkhachhang;

    public String getTenkhachhang() {
		return tenkhachhang;
	}

	public void setTenkhachhang(String tenkhachhang) {
		this.tenkhachhang = tenkhachhang;
	}

	public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}