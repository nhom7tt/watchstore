package com.example.OrderService.entity;

import java.util.Set;

import com.example.OrderService.entity.Order;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name = "watch")
public class Watch {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int watch_id;
    private String tensampham;
    private double gia;
    private int soluong;
    private String xuatsu;

    @ManyToOne
    @JoinColumn(name = "order_id")
    private Order order;

//    public Order getOrder() {
//        return order;
//    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public int getWatch_id() {
        return watch_id;
    }

    public void setWatch_id(int watch_id) {
        this.watch_id = watch_id;
    }

    public String getTensampham() {
        return tensampham;
    }

    public void setTensampham(String tensampham) {
        this.tensampham = tensampham;
    }

    public double getGia() {
        return gia;
    }

    public void setGia(double gia) {
        this.gia = gia;
    }

//    public int getSoluong() {
//        return soluong;
//    }

    public void setSoluong(int soluong) {
        this.soluong = soluong;
    }

    public String getXuatsu() {
        return xuatsu;
    }

    public void setXuatsu(String xuatsu) {
        this.xuatsu = xuatsu;
    }
}
