package com.example.OrderService.entity;

import java.util.Set;

import com.example.OrderService.entity.Order;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

import jakarta.persistence.Table;


@Entity
@Table(name = "customer")

public class Customer {
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
 private int id;
 private String tenkhachhang;
//public int getId() {
//	return id;
//}
public void setId(int id) {
	this.id = id;
}
public String getTenkhachhang() {
	return tenkhachhang;
}
public void setTenkhachhang(String tenkhachhang) {
	this.tenkhachhang = tenkhachhang;
}
 
}
