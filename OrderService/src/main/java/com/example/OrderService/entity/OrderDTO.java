package com.example.OrderService.entity;

import java.util.List;

public class OrderDTO {
	private int order_id;
//    private int soluong;
    private UserDTO user;
    private CustomerDTO customer;
    private List<WatchDTO> watches;

    // Getters and setters
    
//
//    public int getSoluong() {
//        return soluong;
//    }
//
//    public void setSoluong(int soluong) {
//        this.soluong = soluong;
//    }

    public UserDTO getUser() {
        return user;
    }

    public int getOrder_id() {
		return order_id;
	}

	public void setOrder_id(int order_id) {
		this.order_id = order_id;
	}

	public void setUser(UserDTO user) {
        this.user = user;
    }

    public CustomerDTO getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerDTO customer) {
        this.customer = customer;
    }

    public List<WatchDTO> getWatches() {
        return watches;
    }

    public void setWatches(List<WatchDTO> watches) {
        this.watches = watches;
    }
}
