package com.example.OrderService.reponsitory;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.OrderService.entity.User;


public interface UserRepository extends JpaRepository<User, Integer> {

}