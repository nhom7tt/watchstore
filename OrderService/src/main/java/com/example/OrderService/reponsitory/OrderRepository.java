package com.example.OrderService.reponsitory;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.OrderService.entity.Order;


public interface OrderRepository extends JpaRepository<Order, Integer> {
    // No additional methods are needed at this point
}
