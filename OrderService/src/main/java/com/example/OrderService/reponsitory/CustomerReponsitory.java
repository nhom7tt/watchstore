package com.example.OrderService.reponsitory;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.OrderService.entity.Customer;


@Repository
public interface CustomerReponsitory  extends JpaRepository<Customer, Integer>{

}
