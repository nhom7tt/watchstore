package com.example.OrderService.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.example.OrderService.entity.Customer;
import com.example.OrderService.entity.Order;
import com.example.OrderService.entity.OrderDTO;
import com.example.OrderService.entity.User;
import com.example.OrderService.entity.UserDTO;
import com.example.OrderService.entity.Watch;
import com.example.OrderService.entity.WatchDTO;
import com.example.OrderService.reponsitory.WatchReponsitory;
import com.example.OrderService.server.CustomerService;
import com.example.OrderService.server.CustomerServiceImlp;
import com.example.OrderService.server.OrderService;
import com.example.OrderService.server.UserService;
import com.example.OrderService.server.WatchService;




@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderService orderService;
    
    @Autowired 
    private CustomerService customerService;
    
    @Autowired 
    private UserService userService;
    
    @Autowired
    private WatchReponsitory watchReponsitory;
    

    @Autowired
    private RestTemplate restTemplate;

    @RequestMapping(value = "/getOrder", method = RequestMethod.GET)
    public List<Order> getAllOrders() {
        //return orderService.findAllOrders();
    	List<Order> orders = orderService.findAllOrders();
        // Lặp qua từng đơn hàng và lấy thông tin về các mặt hàng tương ứng
        for (Order order : orders) {
        	int orderId = order.getOrder_id();
        	List<Integer> orderIds = Arrays.asList(orderId);
        	List<Watch> watches = watchReponsitory.findAllById(orderIds);
        	order.setWatches(watches);
        }
        return orders;
    	
    }

    @RequestMapping(value = "/postOrder", method = RequestMethod.POST)
    public Order addOrder(@RequestBody OrderDTO orderDTO) {
        if (orderDTO.getWatches() == null || orderDTO.getWatches().isEmpty()) {
            throw new RuntimeException("Watches không có");
        }

        Order order = new Order();
//        order.setSoluong(orderDTO.getSoluong());

        User user = userService.findUser(orderDTO.getUser().getId());
        Customer customer = customerService.findCustomerById(orderDTO.getCustomer().getId());

        if (customer == null || user == null) {
            throw new RuntimeException("Customer or User không có ID đó");
        }
        order.setUser(user);
        order.setCustomer(customer);

        List<Watch> watches = new ArrayList<>();
        for (WatchDTO watchDTO : orderDTO.getWatches()) {
            Watch watch = watchReponsitory.findById(watchDTO.getId()).orElse(null);
            if (watch != null) {
                watches.add(watch);
                
            } else {
                throw new RuntimeException("Watch không có ID: " + watchDTO.getId());
            }
        }

        order.setWatches(watches);
        return orderService.addOrder(order);
    }

 
    @RequestMapping(value = "/putOrder", method = RequestMethod.PUT)
//    public Order updateOrder(@RequestBody Order order) {
//        return orderService.updateOrder(order);
//    }
    public Order updateOrder(@RequestBody OrderDTO orderDTO) {
        Order existingOrder = orderService.findOrderById(orderDTO.getOrder_id());
        
        if (existingOrder == null) {
            throw new RuntimeException("Order không có ID đó: " + orderDTO.getOrder_id());
        }

        if (orderDTO.getWatches() == null || orderDTO.getWatches().isEmpty()) {
            throw new RuntimeException("Watches không có");
        }

        User user = userService.findUser(orderDTO.getUser().getId());
        Customer customer = customerService.findCustomerById(orderDTO.getCustomer().getId());

        if (customer == null || user == null) {
            throw new RuntimeException("Customer hoặc User không có ID đó");
        }

        existingOrder.setUser(user);
        existingOrder.setCustomer(customer);

        List<Watch> watches = new ArrayList<>();
        for (WatchDTO watchDTO : orderDTO.getWatches()) {
            Watch watch = watchReponsitory.findById(watchDTO.getId()).orElse(null);
            if (watch != null) {
                watches.add(watch);
            } else {
                throw new RuntimeException("Watch không có ID: " + watchDTO.getId());
            }
        }

        existingOrder.setWatches(watches);

        return orderService.updateOrder(existingOrder);
    }


    @RequestMapping(value = "/deleteOrder/{id}", method = RequestMethod.DELETE)
    public void deleteOrder(@PathVariable int id) {
        orderService.deleteOrder(id);
    }

    @RequestMapping(value = "/findOrder/{id}", method = RequestMethod.GET)
    public Order findOrder(@PathVariable int id) {
        return orderService.findOrderById(id);
    }
}
