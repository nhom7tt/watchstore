package com.example.OrderService.server;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.OrderService.entity.Watch;
import com.example.OrderService.reponsitory.WatchReponsitory;




@Service
public class WatchServiceImpl implements WatchService {
    @Autowired
    WatchReponsitory watchRepository;

    @Override
    public Watch findWatch(int watch_id) {
        return watchRepository.findById(watch_id).orElse(null);
    }
}
