package com.example.OrderService.server;

import java.util.List;

import com.example.OrderService.entity.Customer;



public interface CustomerService   {
    Customer findCustomerById(int id);
}
