package com.example.OrderService.server;

import java.util.List;


import com.example.OrderService.entity.Order;

public interface OrderService {
	List<Order> findAllOrders();
	Order addOrder(Order order );
	Order updateOrder(Order order);
    void deleteOrder(int Order_id);
    Order findOrderById(int Order_id);
}
