package com.example.OrderService.server;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.example.OrderService.entity.User;
import com.example.OrderService.reponsitory.UserRepository;




@Service
public class UserServiceIml implements UserService {

	@Autowired
	private UserRepository userRepository;


	@Autowired
	private JdbcTemplate jdbcTemplate;



	@Override
	public User findUser(int id) {
		// TODO Auto-generated method stub
		return userRepository.findById(id).orElse(null);
	}


}
