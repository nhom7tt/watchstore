package com.example.OrderService.server;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.example.OrderService.entity.Customer;
import com.example.OrderService.entity.Order;
import com.example.OrderService.reponsitory.OrderRepository;

@Service
public class OrderServerImp implements OrderService {

	   @Autowired
	    private OrderRepository orderRepository;
	   @Autowired
	    private RestTemplate restTemplate;
	
	@Override
	public List<Order> findAllOrders() {
		// TODO Auto-generated method stub
		List<Order> orders = new ArrayList<>();
        orders = orderRepository.findAll();
        return orders;
	}

	@Override
	public Order addOrder(Order order) {
		// TODO Auto-generated method stub
		
		 return orderRepository.saveAndFlush(order);
	}

	@Override
	public Order updateOrder(Order order) {
		// TODO Auto-generated method stub
		Optional<Order> existingOrderOptional = orderRepository.findById(order.getOrder_id());
        if (existingOrderOptional.isPresent()) {
            Order existingOrder = existingOrderOptional.get();
            // Assuming you need to update the fields of the existing order
            existingOrder.setCustomer(order.getCustomer());
            existingOrder.setUser(order.getUser());
            existingOrder.setWatches(order.getWatches());

            //existingOrder.setSoluong(order.getSoluong());
            return orderRepository.saveAndFlush(existingOrder);
        } else {
            throw new RuntimeException("Order id " + order.getOrder_id() + " not found");
        }
	}

	@Override
	public void deleteOrder(int Order_id) {
		// TODO Auto-generated method stub
		orderRepository.deleteById(Order_id);
	}

	@Override
	public Order findOrderById(int Order_id) {
		// TODO Auto-generated method stub
		return orderRepository.findById(Order_id).orElse(null);
	}


}
